arquivo 
\platforms\android\local.properties
add path to android sdk 
ex: sdk.dir=C:/Users/PerformaIT/AppData/Local/Android/Sdk

generate build apk
run \platforms\android\gradlew.bat bundle

sign bundle 
/platforms/android/app/build/outputs/bundle/release
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore .\logarexpress.keystore .\app-release.aab logarexpress
senha da chave: logar@2022

obs
targetSdkVersion="28" => targetSdkVersion="30"
versionCode="1" => versionCode="4" ++
versionName="0.0.1" => versionName="0.0.4" ++
package="io.ionic.starter" => package="com.logarexpress.logar"
