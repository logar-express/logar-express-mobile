import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { FavoritosModule } from './favoritos/favoritos.module';
import { FretesRoutingModule } from './fretes/fretes-routing.module';
import { HomeModule } from './home/home.module';
import { LoginRoutingModule } from './login/login-routing.module';
import { PublicidadeRoutingModule } from './publicidade/publicidade-routing.module';
import { AuthGuard } from './shared/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => LoginRoutingModule,
  },
  {
    path: 'home',
    loadChildren: () => HomeModule,
    canActivate: [AuthGuard],
  },
  {
    path: 'favoritos',
    loadChildren: () => FavoritosModule,
    canActivate: [AuthGuard]
  },
  {
    path: 'fretes',
    loadChildren: () => FretesRoutingModule,
    canActivate: [AuthGuard],
  },
  {
    path: 'mercado-caminhoneiro',
    loadChildren: () => PublicidadeRoutingModule,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
