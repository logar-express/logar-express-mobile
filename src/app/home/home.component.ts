import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  funcionalidadesDisponiveis = [
    {
      titulo: 'Ver Fretes',
      caminhoIcone: './assets/icon/ver-fretes-icone.svg',
      rota: '/fretes',
    },
    {
      titulo: 'Mercado Caminhoneiro',
      caminhoIcone: './assets/icon/mercado-caminhoneiro-home-icone.svg',
      rota: '/mercado-caminhoneiro',
    },
  ];

  constructor() {}

  ngOnInit() {}

  suporteWhatsapp() {
    const a = document.createElement('a');
    a.href = `https://api.whatsapp.com/send?phone=554130409538`;
    a.target = '_blank';
    a.click();
    a.remove();
  }
}
