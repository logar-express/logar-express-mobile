import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FretesComponent } from './fretes.component';
import { FretesRoutingModule } from './fretes-routing.module';
import { SharedModule } from '../shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { FretesListagemComponent } from './components/fretes-listagem/fretes-listagem.component';
import { FretesFiltroComponent } from './components/fretes-filtro/fretes-filtro.component';
import { FreteDetalheComponent } from './components/frete-detalhe/frete-detalhe.component';
import { AngularMaterialModule } from '../shared/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    FretesRoutingModule,
    SharedModule,
    IonicModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule,
  ],
  declarations: [
    FretesComponent,
    FretesListagemComponent,
    FretesFiltroComponent,
    FreteDetalheComponent,
  ],
})
export class FretesModule {}
