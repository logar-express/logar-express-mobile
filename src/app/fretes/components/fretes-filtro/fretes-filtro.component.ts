import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime, map, startWith } from 'rxjs/operators';
import { FiltroFretes } from 'src/app/shared/data/fretes.interface';

@Component({
  selector: 'app-fretes-filtro',
  templateUrl: './fretes-filtro.component.html',
  styleUrls: ['./fretes-filtro.component.scss'],
})
export class FretesFiltroComponent implements OnInit, OnChanges {
  filtroOpen = false;

  filteredOptionsOrigem: Observable<string[]>;
  filteredOptionsDestino: Observable<string[]>;

  @Input() opcoesCarrocerias: string[] = [];
  @Input() opcoesVeiculos: string[] = [];
  @Input() opcoesDestino: string[] = [];
  @Input() opcoesOrigem: string[] = [];

  @Input() buscarFretesForm: FormGroup;

  @Output() filtrarFretesEvent = new EventEmitter<FiltroFretes>();

  constructor() {}

  ngOnInit() {}

  ngOnChanges(): void {
    this.filteredOptionsOrigem = this.buscarFretesForm
      .get('cidade_coleta')
      .valueChanges.pipe(
        startWith(''),
        map((value) => {
          return this._filterOrigem(value);
        })
      );

    this.filteredOptionsDestino = this.buscarFretesForm
      .get('cidade_entrega')
      .valueChanges.pipe(
        startWith(''),
        map((value) => {
          return this._filterDestino(value);
        })
      );
  }

  filtrarFretes(filtros: FiltroFretes): void {
    this.filtrarFretesEvent.emit(filtros);
  }

  limparFiltros(): void {
    window.location.reload();
  }

  private _filterOrigem(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.opcoesOrigem.filter((option) =>
      option.toLowerCase().includes(filterValue)
    );
  }

  private _filterDestino(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.opcoesDestino.filter((option) =>
      option.toLowerCase().includes(filterValue)
    );
  }
}
