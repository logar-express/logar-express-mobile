import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { IFrete } from 'src/app/shared/data/fretes.interface';
import { FretesService } from 'src/app/shared/services/fretes.service';

@Component({
  selector: 'app-frete-detalhe',
  templateUrl: './frete-detalhe.component.html',
  styleUrls: ['./frete-detalhe.component.scss'],
})
export class FreteDetalheComponent implements OnInit {
  freteId?: number;
  frete: IFrete;

  constructor(
    private activatedRoute: ActivatedRoute,
    private fretesService: FretesService,
    private route: Router,
    public modalService: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: IFrete
  ) {}

  ngOnInit() {
    this.frete = this.data;
    // this.freteId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    // if (this.freteId) {
    //   this.carregarDetalhesFrete(this.freteId);
    // }
  }

  voltar(): void {
    // this.route.navigateByUrl('/fretes');
  }

  carregarDetalhesFrete(freteId: number) {
    this.fretesService.detalhesFrete(freteId).subscribe((detalheFete) => {
      this.frete = detalheFete;
    });
  }

  contato() {
    const a = document.createElement('a');
    a.href = `tel:+55${this.frete.celular_coleta}`;
    a.target = '_blank';
    a.click();
    a.remove();
  }

  whatsapp() {
    const a = document.createElement('a');
    a.href = `https://api.whatsapp.com/send?phone=55${this.frete.celular_coleta}`;
    a.target = '_blank';
    a.click();
    a.remove();
  }
}
