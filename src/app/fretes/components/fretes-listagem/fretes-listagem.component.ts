import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { debounceTime } from 'rxjs/operators';
import {
  FiltroFretes,
  IFrete,
  RespostaListagemFretes,
} from 'src/app/shared/data/fretes.interface';
import { FavoritosService } from 'src/app/shared/services/favoritos.service';
import { FretesService } from 'src/app/shared/services/fretes.service';
import { FreteDetalheComponent } from '../frete-detalhe/frete-detalhe.component';

@Component({
  selector: 'app-fretes-listagem',
  templateUrl: './fretes-listagem.component.html',
  styleUrls: ['./fretes-listagem.component.scss'],
})
export class FretesListagemComponent implements OnInit {
  fretes: IFrete[] = [];
  opcoesCarrocerias: string[] = [];
  opcoesVeiculos: string[] = [];
  opcoesDestino: string[] = [];
  opcoesOrigem: string[] = [];
  favoritos = new Set();

  filtrado = false;
  ordenacaoListagem = 0;
  opcoesOrdenacao = [
    {
      descricao: 'Ordenar por: data coleta',
      valor: 0,
    },
    {
      descricao: 'Ordenar por: mais antigos',
      valor: 1,
    },
    {
      descricao: 'Ordenar por: peso',
      valor: 2,
    },
  ];

  buscarFretesForm = new FormGroup({
    cidade_coleta: new FormControl(),
    cidade_entrega: new FormControl(),
    tipo_carroceria: new FormControl(),
    tipo_veiculo: new FormControl(),
    possui_rastreio: new FormControl(),
    possui_movp: new FormControl(),
  });

  constructor(
    private fretesService: FretesService,
    private modalService: MatDialog,
    private favoritosService: FavoritosService
  ) {}

  ngOnInit() {
    this.carregarDados();

    this.buscarFretesForm.valueChanges
      .pipe(debounceTime(2500))
      .subscribe((filtros: FiltroFretes) => this.filtrarFretes(filtros));
  }

  carregarDados() {
    this.filtrado
      ? this.filtrarFretes(this.buscarFretesForm.getRawValue())
      : this.getFretes();
    this.getFavoritos();
  }

  getFavoritos() {
    this.favoritosService.listarFavoritos().subscribe((resposta) => {
      resposta.favoritos.forEach((favorito) => {
        this.favoritos.add(favorito.id);
      });
    });
  }

  removerFavorito(freteId: number) {
    this.favoritosService.removerFavorito(freteId).subscribe(() => {
      this.favoritos = new Set();
      // this.fretes = [];
      this.carregarDados();
    });
  }

  adicionarFavorito(freteId: number) {
    this.favoritosService.adicionarFavorito(freteId).subscribe(() => {
      this.favoritos = new Set();
      // this.fretes = [];
      this.carregarDados();
    });
  }

  filtrarFretes(filtros: FiltroFretes): void {
    this.filtrado = true;

    this.fretesService
      .filtrarFretes(filtros)
      .subscribe((resposta) => this.formatarRespostaFrete(resposta));
  }

  getFretes() {
    this.fretesService.getAll().subscribe((resposta) => {
      this.formatarRespostaFrete(resposta);
    });
  }

  formatarRespostaFrete(resposta: RespostaListagemFretes): void {
    this.fretes = resposta.fretes;
    this.reordenarListagem();

    this.opcoesCarrocerias = [
      ...new Set(
        resposta.carrocerias.map((carroceria) => carroceria.tipo_carroceria)
      ),
    ];
    const veiculos = [
      ...new Set(
        resposta.veiculos.map((veiculo) => {
          return veiculo.tipo_veiculo.split(', ');
        })
      ),
    ];

    this.opcoesVeiculos = [
      ...new Set(
        veiculos.flat(1).sort(function (a, b) {
          if (a < b) {
            return -1;
          }
          if (a > b) {
            return 1;
          }
          return 0;
        })
      ),
    ];

    this.opcoesDestino = [
      ...new Set(
        resposta.destino.map(
          (destino) => `${destino.cidade_entrega} - ${destino.estado_entrega}`
        )
      ),
    ];

    this.opcoesOrigem = [
      ...new Set(
        resposta.origem.map(
          (origem) => `${origem.cidade_coleta} - ${origem.estado_coleta}`
        )
      ),
    ];
  }

  verDetalhesFrete(frete: IFrete) {
    this.modalService.open(FreteDetalheComponent, {
      data: frete,
      width: '90vw',
      height: '95vh',
      autoFocus: false,
    });
  }

  reordenarListagem(): void {
    switch (this.ordenacaoListagem) {
      case 0:
        this.fretes = this.fretes.sort((freteA, freteB) => {
          if (freteA.data_coleta < freteB.data_coleta) {
            return -1;
          }
          if (freteA.data_coleta > freteB.data_coleta) {
            return 1;
          }
          return 0;
        });
        break;
      case 1:
        this.fretes = this.fretes.sort((freteA, freteB) => {
          if (freteA.created_at < freteB.created_at) {
            return -1;
          }
          if (freteA.created_at > freteB.created_at) {
            return 1;
          }
          return 0;
        });
        break;
      case 2:
        this.fretes = this.fretes.sort((freteA, freteB) => {
          if (freteA.peso_toneladas < freteB.peso_toneladas) {
            return -1;
          }
          if (freteA.peso_toneladas > freteB.peso_toneladas) {
            return 1;
          }
          return 0;
        });
        break;
      default:
        break;
    }
  }
}
