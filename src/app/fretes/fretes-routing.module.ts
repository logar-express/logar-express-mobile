import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FreteDetalheComponent } from './components/frete-detalhe/frete-detalhe.component';
import { FretesListagemComponent } from './components/fretes-listagem/fretes-listagem.component';
import { FretesComponent } from './fretes.component';

const routes: Routes = [
  {
    path: '',
    component: FretesComponent,
    children: [
      { path: '', component: FretesListagemComponent },
      { path: ':id', component: FreteDetalheComponent }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FretesRoutingModule {}
