import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { FreteDetalheComponent } from '../fretes/components/frete-detalhe/frete-detalhe.component';
import {
  IFrete,
  RespostaListagemFretes,
} from '../shared/data/fretes.interface';
import { FavoritosService } from '../shared/services/favoritos.service';
import { FretesService } from '../shared/services/fretes.service';

@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.component.html',
  styleUrls: ['./favoritos.component.scss'],
})
export class FavoritosComponent implements OnInit {
  favoritos = new Set();
  fretes: IFrete[] = [];

  constructor(
    private favoritosService: FavoritosService,
    public dialog: MatDialog,
    public shippingService: FretesService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.carregarDados();
  }

  carregarFavoritos() {
    this.favoritosService.listarFavoritos().subscribe((resposta) => {
      resposta.favoritos.forEach((favorito) => {
        this.favoritos.add(favorito.id);
      });
    });
  }

  carregarDados() {
    this.getFretes();
    this.carregarFavoritos();
  }

  removerFavorito(freteId: number) {
    this.favoritosService.removerFavorito(freteId).subscribe(() => {
      this.favoritos = new Set();
      this.fretes = [];
      this.carregarDados();
    });
  }

  adicionarFavorito(freteId: number) {
    this.favoritosService.adicionarFavorito(freteId).subscribe(() => {
      this.favoritos = new Set();
      this.fretes = [];
      this.carregarDados();
    });
  }

  openDialog(frete: IFrete) {
    // this.route.navigateByUrl(`/fretes/${frete.id}`);
    this.dialog.open(FreteDetalheComponent, {
      data: frete,
      width: '95vw',
      autoFocus: false,
    });
  }

  getImageLink(frete: IFrete) {
    return `${environment.baseUrl}/imagem/${frete.logo}`;
  }

  getFretes() {
    this.shippingService
      .getAll()
      .subscribe((resposta) => this.formatarRespostaFrete(resposta));
  }

  formatarRespostaFrete(resposta: RespostaListagemFretes): void {
    this.fretes = resposta.fretes;
  }
}
