import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicidadeListagemComponent } from './publicidade-listagem/publicidade-listagem.component';
import { PublicidadeComponent } from './publicidade.component';

const routes: Routes = [
  {
    path: '',
    component: PublicidadeComponent,
    children: [{ path: '', component: PublicidadeListagemComponent }],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicidadeRoutingModule {}
