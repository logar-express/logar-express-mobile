import { Component, Input, OnInit } from '@angular/core';
import { IAnuncio } from 'src/app/shared/data/anuncio.interface';
import { RoleEnum } from 'src/app/shared/data/role.enum';
import { AnuncioService } from 'src/app/shared/services/anuncio.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-publicidades-item-listagem',
  templateUrl: './publicidades-item-listagem.component.html',
  styleUrls: ['./publicidades-item-listagem.component.scss'],
})
export class PublicidadesItemListagemComponent implements OnInit {
  @Input() publicidade!: IAnuncio;
  isAdmin = false;
  isAnunciante = false;

  constructor(
    private authService: AuthService,
    private anunciosService: AnuncioService
  ) {}

  ngOnInit() {
    this.isAdmin = this.authService.getRole() === RoleEnum.Admin;
    this.isAnunciante = this.authService.getRole() === RoleEnum.Anunciante;
  }

  getImageLink() {
    return `${environment.baseUrl}/imagem/${this.publicidade.caminho_arquivo}`;
  }

  comprarProduto() {
    const a = document.createElement('a');
    a.href = this.publicidade.url;
    a.target = '_blank';
    a.click();
    a.remove();
  }
}
