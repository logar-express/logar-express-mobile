import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicidadeComponent } from './publicidade.component';
import { PublicidadeRoutingModule } from './publicidade-routing.module';
import { PublicidadeListagemComponent } from './publicidade-listagem/publicidade-listagem.component';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../shared/shared.module';
import { PublicidadesItemListagemComponent } from './publicidades-item-listagem/publicidades-item-listagem.component';

@NgModule({
  imports: [CommonModule, PublicidadeRoutingModule, SharedModule, IonicModule],
  declarations: [PublicidadeComponent, PublicidadeListagemComponent, PublicidadesItemListagemComponent],
})
export class PublicidadeModule {}
