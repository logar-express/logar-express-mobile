import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IAnuncio } from 'src/app/shared/data/anuncio.interface';
import { AnuncioService } from 'src/app/shared/services/anuncio.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-publicidade-listagem',
  templateUrl: './publicidade-listagem.component.html',
  styleUrls: ['./publicidade-listagem.component.scss'],
})
export class PublicidadeListagemComponent implements OnInit {
  anunciosVip: IAnuncio[] = [];
  anuncios: IAnuncio[] = [];

  constructor(
    private router: Router,
    private anunciosService: AnuncioService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.getAnuncios();
  }

  getImageLink(publicidade: IAnuncio) {
    return publicidade.imagem_vip
      ? `${environment.baseUrl}/imagem/${publicidade.imagem_vip}`
      : `${environment.baseUrl}/imagem/${publicidade.caminho_arquivo}`;
  }

  goToUrl(anuncio: IAnuncio) {
    const a = document.createElement('a');
    a.href = anuncio.url;
    a.target = '_blank';
    a.click();
  }

  getAnuncios() {
    this.anunciosService.listarTodosAnuncios().subscribe((res) => {
      this.anuncios = res.publicidades;
    });

    this.anunciosService.listarAnunciosVip().subscribe((res) => {
      this.anunciosVip = res.publicidades;
    });
  }
}
