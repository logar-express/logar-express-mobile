import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../shared/services/auth.service';
import { LoginService } from '../shared/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  formularioLogin = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
    ]),
  });

  constructor(
    private toastService: ToastrService,
    private loginService: LoginService,
    private loadingController: LoadingController,
    private authService: AuthService,
    private route: Router
  ) {}

  ngOnInit() {
    const session = this.authService.getSession();

    if (session) {
      const isExpired = new Date(session.expires_in) < new Date();

      if (!isExpired) {
        this.route.navigateByUrl('/home');
      }
    }
  }

  async fazerLogin() {
    if (this.formularioLogin.valid) {
      const loading = await this.loadingController.create({
        message: 'Carregando...',
      });
      await loading.present();

      this.loginService
        .login(this.formularioLogin.getRawValue())
        .add(() => loading.dismiss());
    } else {
      const errors = [];

      if (this.formularioLogin.get('email')?.errors?.required) {
        errors.push('O campo email é obrigatório.');
      }

      if (this.formularioLogin.get('email')?.errors?.email) {
        errors.push('O email informado não é um email válido.');
      }

      if (this.formularioLogin.get('password')?.errors?.minlength) {
        console.log(this.formularioLogin.get('password')?.errors);
        errors.push(`O campo precisa ter no mínimo
        ${
          this.formularioLogin.get('password')?.errors?.minlength.requiredLength
        }
        caracteres e tem
        ${this.formularioLogin.get('password')?.errors?.minlength.actualLength}
        caracteres.`);
      }

      if (this.formularioLogin.get('password')?.errors?.required) {
        errors.push('O campo senha é obrigatório.');
      }

      errors.forEach((erro) => {
        this.toastService.error(erro);
      });
    }
  }

  acessarCadastro() {
    const a = document.createElement('a');
    a.href = 'https://logar.logarexpress.com/cadastro';
    a.target = '_blank';
    a.click();
  }

  acessarEsqueciSenha() {
    const a = document.createElement('a');
    a.href = 'https://logar.logarexpress.com/recuperar-senha';
    a.target = '_blank';
    a.click();
  }
}
