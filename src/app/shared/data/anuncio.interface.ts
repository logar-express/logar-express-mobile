export interface IAnuncio {
  id: number;
  titulo: string;
  segmento: string;
  name: string;
  preco: number;
  descricao: string;
  url: string;
  tempo_dias: number;
  caminho_arquivo: string;
  imagem_vip?: string;
  publicidade_aprovada: number;
  codigo_publicidade: string;
}
