export interface RespostaListagemFretes {
  carrocerias: { tipo_carroceria: string }[];
  destino: { cidade_entrega: string; estado_entrega: string }[];
  fretes: IFrete[];
  origem: { cidade_coleta: string; estado_coleta: string }[];
  veiculos: { tipo_veiculo: string }[];
}

export interface FiltroFretes {
  cidade_coleta: string;
  cidade_entrega: string;
  tipo_carroceria: string;
  tipo_veiculo: string;
  possui_rastreio: boolean;
  possui_movp: boolean;
}

export interface IFrete {
  frete_aprovado: number;
  tipo_veiculo: string;
  tipo_carreta: string;
  tipo_carroceria: string;
  dataDias: number;
  dimensoes_minimas: string;
  requisitos_extras: string;
  tipo_produto: string;
  acondicionamento_produto: string;
  nome_responsavel_coleta: string;
  data_coleta: string;
  celular_coleta: string;
  endereco_coleta: string;
  cidade_coleta: string;
  estado_coleta: string;
  valor_frete: number;
  peso_toneladas: number;
  nome_responsavel_entrega: string;
  data_entrega: string;
  celular_entrega: string;
  endereco_entrega: string;
  cidade_entrega: string;
  estado_entrega: string;
  codigo_frete: string;
  logo: string;
  razao_social: string;
  created_at: string;
  id?: number;
}
