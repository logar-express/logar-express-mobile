export enum RoleEnum {
  Anunciante = 'Anunciante',
  Transportadora = 'Transportadora',
  Caminhoneiro = 'Caminhoneiro',
  Admin = 'Administrador',
}
