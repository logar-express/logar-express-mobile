export interface IUser {
  name: string;
  id: string;
}

export interface IToken {
  access_token: string;
  token_type: string;
  expires_in: number;
  dados: ITransportadora | ICaminhoneiro;
  role: string;
}

export interface ITransportadora {
  id: string;
  cnpj: string;
  razao_social: string;
  endereco_comercial: string;
  cidade: string;
  estado: string;
  cep: string;
  telefone: string;
  celular: string;
  logo: string;
  nome_funcionario: string;
  cargo: string;
  id_usuario: string;
}

export interface ICaminhoneiro {
  id: number;
  possui_mopp: boolean;
  ano: string;
  celular: string;
  cnh: string;
  cor: string;
  cpf: string;
  data_emissao_cnh: string;
  estado_expeditor_cnh: string;
  marca: string;
  modelo: string;
  nome: string;
  placa: string;
  renavam: string;
  telefone: string;
  tipo_carreta: string;
  tipo_carroceria: string;
  tipo_veiculo: string;
}
