import { ICaminhoneiro } from './user.interface';

export interface ISession {
  access_token: string;
  token_type: string;
  expires_in: number;
  dados: ICaminhoneiro;
  role: string;
}
