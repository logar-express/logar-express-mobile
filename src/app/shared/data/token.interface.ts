import { ICaminhoneiro, ITransportadora } from "./user.interface";

export interface IToken {
  access_token: string;
  token_type: string;
  expires_in: number;
  dados: ITransportadora | ICaminhoneiro;
  role: string;
}
