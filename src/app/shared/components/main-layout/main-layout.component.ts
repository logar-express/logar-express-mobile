import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent implements OnInit {
  menuOpen = false;

  menuItens = [
    {
      icone: './assets/icon/home.svg',
      descricao: 'Início',
      rota: '/home',
    },
    {
      icone: './assets/icon/frete-icon.svg',
      descricao: 'Fretes',
      rota: '/fretes',
    },
    {
      icone: './assets/icon/mercado-caminhoneiro-icon.svg',
      descricao: 'Mercado Caminhoneiro',
      rota: '/mercado-caminhoneiro',
    },
  ];

  username?: string;

  constructor(
    private sessionService: SessionService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.username = this.authService.getSession().dados.nome.split(' ')[0];
  }

  logout() {
    this.sessionService.logout();
  }
}
