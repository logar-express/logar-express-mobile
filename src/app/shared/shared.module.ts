import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AngularMaterialModule } from './angular-material.module';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';

@NgModule({
  imports: [IonicModule, CommonModule, RouterModule, AngularMaterialModule],
  exports: [MainLayoutComponent],
  declarations: [MainLayoutComponent],
  providers: [],
})
export class SharedModule {}
