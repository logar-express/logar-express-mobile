import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ISession } from '../data/session.interface';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  constructor(private router: Router) {}

  setSession(session: ISession) {
    localStorage.setItem(
      `${environment.prefix}_session`,
      JSON.stringify(session)
    );
  }

  reset() {
    localStorage.clear();
  }

  logout() {
    this.reset();

    this.router.navigate(['/']);
  }
}
