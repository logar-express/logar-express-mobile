import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IFrete } from '../data/fretes.interface';

@Injectable({
  providedIn: 'root'
})
export class FavoritosService {
  private baseUrlFavoritos = `${environment.baseUrl}/favoritos`;

  constructor(private httpClient: HttpClient) {}

  listarFavoritos() {
    return this.httpClient.get<{ favoritos: IFrete[] }>(
      `${this.baseUrlFavoritos}`
    );
  }

  adicionarFavorito(frete_id: number) {
    return this.httpClient.post<{ favoritos: IFrete[] }>(
      `${this.baseUrlFavoritos}`,
      { frete_id }
    );
  }

  removerFavorito(frete_id: number) {
    return this.httpClient.delete<{ favoritos: IFrete[] }>(
      `${this.baseUrlFavoritos}/${frete_id}`
    );
  }
}
