import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {
  FiltroFretes,
  IFrete,
  RespostaListagemFretes,
} from '../data/fretes.interface';

@Injectable({
  providedIn: 'root',
})
export class FretesService {
  private api = 'frete';
  private baseUrl = `${environment.baseUrl}/${this.api}`;

  constructor(private httpClient: HttpClient) {}

  filtrarFretes(filtros: FiltroFretes) {
    return this.httpClient.post<RespostaListagemFretes>(
      `${environment.baseUrl}/filtro-frete`,
      filtros
    );
  }

  detalhesFrete(freteId: number) {
    return this.httpClient.get<IFrete>(`${this.baseUrl}/${freteId}`);
  }

  getAll() {
    return this.httpClient.get<RespostaListagemFretes>(`${this.baseUrl}`);
  }
}
