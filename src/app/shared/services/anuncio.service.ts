import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IAnuncio } from '../data/anuncio.interface';

@Injectable({
  providedIn: 'root',
})
export class AnuncioService {
  private baseUrlPublicidade = `${environment.baseUrl}/publicidade`;

  constructor(private httpClient: HttpClient) {}

  listarTodosAnuncios() {
    return this.httpClient.get<{ publicidades: IAnuncio[] }>(
      this.baseUrlPublicidade
    );
  }

  listarAnunciosVip() {
    return this.httpClient.get<{ publicidades: IAnuncio[] }>(
      `${this.baseUrlPublicidade}-vip`
    );
  }
}
