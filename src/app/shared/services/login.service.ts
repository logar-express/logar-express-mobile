import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { RoleEnum } from '../data/role.enum';
import { ISession } from '../data/session.interface';
import { IToken } from '../data/user.interface';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  private api = 'auth';
  private baseUrl = `${environment.baseUrl}/${this.api}`;

  constructor(
    private httpClient: HttpClient,
    private sessionService: SessionService,
    private router: Router,
    private toastService: ToastrService
  ) {}

  login(form: { email: string; password: string }) {
    return this.httpClient.post<IToken>(`${this.baseUrl}/login`, form).subscribe(
      (res) => {
        const user = res.dados;

        const session = {
          access_token: res.access_token,
          expires_in: Date.now() + res.expires_in * 1000,
          dados: user,
          role: res.role,
        } as ISession;

        this.sessionService.setSession(session);

        const role = session.role;

        role === RoleEnum.Caminhoneiro
          ? this.router.navigate(['/home'])
          : this.toastService.error(
              'Você não pode fazer login neste aplicativo. Por favor use a versão web.'
            );
      },
      (err) => {
        let message: string;

        if (err.status === 401) {
          message = 'Login/senha inválidos.';
        } else {
          message = err.message;
        }

        this.toastService.error(message);
        console.log('err', err);
      }
    );
  }
}
